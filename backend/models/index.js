'use strict'
import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const Schema = mongoose.Schema;

const url = 'mongodb://localhost/got';

const CharacterSchema = new Schema({
  titles: Array,
  books: Array,
  birth: Number,
  pagerank: Object,
  image: String,
  gender: String,
  culture: Array,
  house: String,
  slug: String,
  name: String,
});

CharacterSchema.plugin(mongoosePaginate);

export default mongoose.model('character', CharacterSchema);