import Character from './models';
import mongoose from 'mongoose';
import express from 'express';

const router = express.Router();

router.get('/character', async function(req, res) {
  const { search, page } = req.query;
  const searchData = new RegExp(search, "i");
  console.log(searchData);
  console.log(page);
  const data = await Character.paginate({ $or : [ {'name': searchData}, {'house': searchData } ] } , 
    { page, limit: 10 });
  //Character.find({$or:[ {'name': searchData}, {'house': searchData }]});
  res.json(data)
});
router.get('/character/:id', async function(req, res) {
  let data = await Character.findOne({_id: mongoose.Types.ObjectId(req.params.id)});
  res.json(data)
});
export default router;