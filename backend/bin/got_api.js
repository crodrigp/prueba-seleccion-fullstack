import Request from 'request';
import Character from '../models';


class GOTAPI {
  async init(){
    const lista = await Character.count();
    if (lista == 0){
      await this.insertData(process.env.URL);
      await this.insertData(process.env.URL2);
    }
  }
  async insertData(url){
    const data = await this.callAPI(url);
    data.map((value) => {
      const character = new Character(value);
      character.save();
    });
  }
  callAPI(url){
    return new Promise(function (resolve, reject) {
      Request.get(url, function(error, res, body){
        if ( !error && res.statusCode == 200 ){
          resolve(JSON.parse(body));
        }else{
          reject(error);
        }
      });
    });
  }
}

export default GOTAPI;