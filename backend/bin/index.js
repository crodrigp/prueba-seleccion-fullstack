import mongoose from 'mongoose';
import GOTAPI from './got_api';
import app from './server';

const url = 'mongodb://localhost/got';

mongoose.connect(url, function(err, db){
  if (err) throw err;
  const gotApi = new GOTAPI();
  gotApi.init();
  console.log("Connected to db");
});