import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';

class Search extends Component{
  constructor(props){
    super(props);
    this.state = {
      handleChange: props.handleChange,
      value: '',
    }
  }
  changeSearch = (e) => {
    this.setState({value: e.target.value});
    this.state.handleChange(e.target.value);
  }
  render(){
    return (
      <>
        <input type='text' value={this.state.value} onChange={this.changeSearch}/>
        <Button onClick={ () => this.state.handleChange(this.state.value) }>Buscar</Button>
      </>
    );
  }
}

export default Search;
