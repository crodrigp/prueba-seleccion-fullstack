import React, {Component} from 'react';
import './App.css';
import InputSearch from './components/inputSearch';
import Button from 'react-bootstrap/Button';

class ListCharacter extends Component{
  constructor(props){
    super(props);
    this.state  = {
      data: [],
      isLoad: false,
      page: 1,
      value: '',
      //"total":2266,"limit":10,"offset":0,"page":1,"pages":227
    }
    this.url = process.env.REACT_APP_LIST;
    this.getData = this.getData.bind(this);
    this.search = this.search.bind(this);

  }
  getData(page){
    fetch(this.url + '?search='+this.state.value+'&page='+page)
    .then(res => res.json())
    .then( result => { this.setState({data: result.docs, page: result.page, pages: result.pages, isLoad: true}) },
      (error) => {
        console.log(error);
        this.setState({ isLoad: true });
      }
    );
  }
  async componentDidMount() {
    this.getData(1);
  }
  async search(value) {
    this.setState({value});
    this.getData(1);
  }
  paginator (){
    let count = [];
    for ( let i = 1 ; i <= this.state.pages ; i++ ){
      count.push(i);
    }
    console.log(this.state.page);
    return count.map(val => {
      console.log(val);
      if ( val == this.state.page ){
              console.log("No entra aca");
              return (<div className="float-left"><Button className="btn btn-success" onClick={() => {this.getData(val)} }>{val}</Button><div className="float-left">&nbsp;</div></div>);
      }
      else{
        return (<div className="float-left"><Button className="btn btn-secondary" onClick={() => {this.getData(val)} }>{val}</Button><div className="float-left">&nbsp;</div></div>);
      }
    });

  }
  render(){
    const { data, isLoad } = this.state;
    if ( isLoad )
      return (
        <div className="App">
          <InputSearch handleChange={this.search} />
          <table>
            <tbody>
              { data.map( row => (
                  <tr key={row._id}>
                    <td>{row._id}</td>
                    <td>{row.name}</td>
                    <td>{row.house}</td>
                    <td><a href={'/View/' + row._id}>Ver</a></td>
                  </tr>
                ))
            }
            </tbody>
          </table>
          {this.paginator()}
        </div>
      );
    else
      return (<div>Cargando</div>);
  }
}

export default ListCharacter;
