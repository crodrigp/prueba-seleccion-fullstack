import React, {Component} from 'react';

class ViewCharacter extends Component{
  constructor(props){
    super(props);
    this.state  = {
      data: [],
      isLoad: false
    }
    this.url = process.env.REACT_APP_VIEW;
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    fetch(this.url + id)
    .then(res => res.json())
    .then( result => { this.setState({data: result, isLoad: true}) },
      (error) => {
        console.log(error);
        this.setState({ isLoad: true });
      }
    );
  }
  render(){
    const { data, isLoad } = this.state;
    if ( isLoad ){
      return (
        <div className="App">
          <p>Nombre: {data.name}</p>
          <p>Casa: {data.house}</p>
          <p>Sexo: {data.gender==='male'?'Hombre':'Mujer'}</p>
          <p>Slug: {data.slug}</p>
          <p><img src={data.image} alt={data.name} /></p>
          <p>Titulos: { data.titles.map( row => ( <p>{row}</p> )  ) }</p>
          <p>Books: { data.books.map( row => ( <p>{row}</p> )  ) }</p>
        </div>
      );
    }
    else
      return (<div>Cargando</div>);
  }
}

export default ViewCharacter;
